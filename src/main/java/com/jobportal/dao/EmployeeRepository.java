package com.jobportal.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jobportal.model.Employee;
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
  Employee findByEmailId(String Emailid);
}
