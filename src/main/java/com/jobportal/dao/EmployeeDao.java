package com.jobportal.dao;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.jobportal.model.Employee;

@Service
public class EmployeeDao {
	@Autowired
	EmployeeRepository employeeRepository;
	
	BCryptPasswordEncoder bCrypt=new BCryptPasswordEncoder();
	
	public Employee register(Employee emp){
		String bCryptpw=bCrypt.encode(emp.getPassword());
		emp.setPassword(bCryptpw);
		return employeeRepository.save(emp);
	}

}
