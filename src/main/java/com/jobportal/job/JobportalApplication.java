package com.jobportal.job;

import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
@EntityScan(basePackages="com.jobportal.model")
@EnableJpaRepositories(basePackages="com.jobportal.dao")
@SpringBootApplication(scanBasePackages="com.jobportal")
public class JobportalApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobportalApplication.class, args);
	}

}
