package com.jobportal.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jobportal.dao.EmployeeDao;
import com.jobportal.model.Employee;

@RestController
public class EmployeeController {
	@Autowired
	EmployeeDao employeedao;
	
	@PostMapping("EmployeeRegister")
	public Employee employeeRegister(@RequestBody Employee emp){
		return employeedao.register(emp);
		
	}
	
	
}
